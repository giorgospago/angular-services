import { Injectable } from '@angular/core';
import { IProduct } from './interfaces/IProduct';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  products: IProduct[] = [
    { brand: "Sony", model: "RDR HX-950", photoUrl: "https://s.sdgcdn.com/10/2018/01/06248ce55907ac7018c6d8f847f49303c0ae246e_2-600x600.jpg", price: 1100, blackFriday: true },
    { brand: "Samsung", model: "RDR HX-950 GX SSD", photoUrl: "https://www.diktyoshop.gr/wp-content/uploads/2016/08/c10385132-1-600x600.jpg", price: 800, blackFriday: false },
    { brand: "LG", model: "RDR HX-950 HDD SH", photoUrl: "https://i3.wp.com/www.somalishopping.com/wp-content/uploads/2018/01/hisense-55-4k-smart-tv-600x600.jpg", price: 650, blackFriday: true },
    { brand: "Philips", model: "RDR HX-950", photoUrl: "https://www.kotsovolos.gr/site/datafiles/catalog/c10464772-1.jpg", price: 550, blackFriday: false },
    { brand: "Panasonic", model: "RDR HX-950", photoUrl: "https://static.vendora.gr/storage/media/c5/20/c520d668884a7149386b31183b4bae271c845c50_l.jpg", price: 750, blackFriday: true }
  ];

  constructor() { }

  deleteProduct(i: number) {
    this.products.splice(i, 1);
  }

  createProduct(form: IProduct) {
    this.products.push(form);
  }

}
