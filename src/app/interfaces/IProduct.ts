export interface IProduct {
  photoUrl: string;
  blackFriday: boolean;
  brand: string;
  model: string;
  price: string;
}
