import { Component, OnInit, Input } from '@angular/core';
import { IProduct } from '../../interfaces/IProduct';
import { ProductsService } from '../../products.service';

@Component({
  selector: 'app-one-product',
  templateUrl: './one-product.component.html',
  styleUrls: ['./one-product.component.css']
})
export class OneProductComponent implements OnInit {

  @Input() product: IProduct;
  @Input() thesi: number;

  constructor(private ps: ProductsService) { }

  ngOnInit() {
  }


  removeProduct() {
    if (confirm("R U SURE ?")) {
      this.ps.deleteProduct(this.thesi);
    }
  }
}
