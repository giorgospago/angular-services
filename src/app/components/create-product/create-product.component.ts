import { Component, OnInit } from '@angular/core';
import { IProduct } from '../../interfaces/IProduct';
import {ProductsService} from '../../products.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  form: IProduct;

  constructor(private ps: ProductsService) {
    this.resetForm();
  }

  ngOnInit() {
  }

  addProduct() {
    this.ps.createProduct(this.form);
    this.resetForm();
  }

  resetForm() {
    this.form = {
      brand: "",
      model: "",
      photoUrl: "",
      price: "",
      blackFriday: false
    };
  }
}
